<h1>Task NOC<h1>

**Instalar e configurar magento2 wordpress tomcat site simples**

- Installacao dos pacotes necessarios 
- Configurar Database
- Configurar PHP-FPM e Magento
- Download Magento
- Configurar Nginx e Magento
- Configurar SELinux e Firewall
- Instalar WordPress
- Instalar e Configurar Tomcat proxy reverso
- Site simples
- Instalar SSL Encrypt


**Install Server**

```
dnf update -y

dnf install wget nginx mariadb-server php php-cli php-mysqlnd php-opcache php-xml php-gd php-soap php-bcmath php-intl php-mbstring php-json php-iconv php-fpm php-zip unzip git -y

systemctl start nginx mariadb php-fpm

systemctl enable nginx mariadb php-fpm
```
> Editar o aquivo  `vim /etc/php.ini` com os parametros abaixo  : 

```
memory_limit =512M
upload_max_filesize = 200M
zlib.output_compression = On 
max_execution_time = 300 
date.timezone = America/Sao_Paulo	
```

**Configurar Database**

> Por padrão, o MariaDB não está protegido, então você precisará protegê-lo primeiro. Execute o seguinte script para garantir a segurança no MariaDB:

`mysql_secure_installation`

> Responda a todas as perguntas como mostrado abaixo:

```
> Enter current password for root (enter for none): 
> Set root password? [Y/n] Y
> New password: 
> Re-enter new password: 
> Remove anonymous users? [Y/n] Y
> Disallow root login remotely? [Y/n] Y
> Remove test database and access to it? [Y/n] Y
> Reload privilege tables now? [Y/n] Y
```
> Uma vez feito isso, faça login no MariaDB com o seguinte comando:

`mysql -u root -p`

> digite sua senha root quando solicitado, em seguida, crie um banco de dados e usuário para o Magento comandos mostrados abaixo:

```
CREATE DATABASE magentodb;

GRANT ALL ON magentodb.* TO magentodb@localhost IDENTIFIED BY 'magentopass@123';

flush privileges;

exit;
```
**Configurar PHP-FPM e Magento**

> Em seguida, você precisará configurar o pool PHP-FPM para a sua instância Magento. Você pode configurá-lo criando o seguinte arquivo `vim  /etc/php-fpm.d/magento.conf`:
> Adicione as seguintes linhas:

```
[magento]
user = nginx
group = nginx
listen.owner = nginx
listen.group = nginx
listen = /run/php-fpm/magento.sock
pm = ondemand
pm.max_children =  50
pm.process_idle_timeout = 10s
pm.max_requests = 500
chdir = /
```

> Salve e feche o arquivo e reinicie o serviço PHP-FPM para implementar as alterações:

`systemctl restart php-fpm`

**Download Magento**

> Primeiro, baixe a versão mais recente do Magento do repositório git usando o seguinte comando:

```
cd /var/www/html
wget https://github.com/magento/magento2/archive/2.3.zip
```

> Uma vez baixado, descompacte o arquivo , mova o diretório extraído para magento2 como mostrado abaixo:

```
unzip 2.3.zip
mv magento2-2.3 magento2
```
> Em seguida, você precisará instalar o Composer para instalar as dependências do PHP para Magento.
> Você pode instalar o Composer com o seguinte comando:

```
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/bin --filename=composer
composer -v
```

> Em seguida, altere o diretório para magento2 e instale todas as dependências do Magento com o seguinte comando:

```
cd /var/www/html/magento2
composer update
composer install
```

> Dê as permissões ao diretório magento2:

```
chown -R nginx:nginx /var/www/html/magento2
chmod -R 755 /var/www/html/magento2
```

**Configurar SELinux e Firewall**

> Em seguida, você precisará criar um arquivo de host virtual Nginx para o Magento. Você pode criá-lo com o seguinte comando:

`vim /etc/nginx/conf.d/magento.conf`

> Adicione as seguintes linhas:

```
upstream fastcgi_backend {
  server   unix:/run/php-fpm/magento.sock;
}

server {
    listen 80;
    server_name loja.brunogomessanches.tk;

    set $MAGE_ROOT /var/www/html/magento2;
    set $MAGE_MODE developer;

    access_log /var/log/nginx/magento-access.log;
    error_log /var/log/nginx/magento-error.log;

    include /var/www/html/magento2/nginx.conf.sample;
}
```
> Salve e feche o arquivo quando terminar. Em seguida, reinicie o serviço Nginx e PHP-FPM para implementar alterações:

`systemctl restart nginx php-fpm`

**Configurar o semanage**

> Por padrão, o SELinux está habilitado no CentOS 8. Assim, você precisará configurar SELinux para o Magento funcionar corretamente.

```
yum install whatprovides semanage
yum install -y policycoreutils-python-utils
semanage -h
semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/magento2(/.*)?'
semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/magento2/app/etc(/.*)?'
semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/magento2/var(/.*)?'
semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/magento2/pub/media(/.*)?'
semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/magento2/pub/static(/.*)?'
restorecon -Rv '/var/www/html/magento2'
```

**Install WordPress**
> 
>Primeiro passo você precisa criar um novo banco de dados MariaDB para WordPress.
> 
>faça login no shell MariaDB com o seguinte comando:

`sudo mysql -u root -p`

> Agora, crie um novo wordpress de banco de dados MariaDB com os comandos SQL abaixo:

```
CREATE DATABASE wordpress;

GRANT ALL ON wordpress.* TO 'wordpress'@'localhost' IDENTIFIED BY 'password';

FLUSH PRIVILEGES;

quit
```

>Baixando e instalando o WordPress

>Navegue ate o diretório `cd /var/www/html/` .

> Agora, baixe o arquivo WordPress mais recente do site oficial do WordPress com o seguinte comando:

`sudo wget https://wordpress.org/latest.tar.gz`

> extraia o arquivo do WordPress latest.tar.gz com o seguinte comando:

`sudo tar xvzf latest.tar.gz`

> Agora, altere o proprietário e o grupo do diretório wordpress / e seu conteúdo para nginx da seguinte maneira:

```
chown -R nginx:nginx /var/www/html/wordpress
chmod -R 755 /var/www/html/wordpress
```
> Se você tiver o SELinux habilitado (o que é muito provável no CentOS 8 / RHEL 8), execute o seguinte comando para definir o contexto SELinux correto para o diretório / var / www / html / wordpress e seu conteúdo.

```
sudo semanage fcontext -a -t httpd_sys_rw_content_t \
"/var/www/html/wordpress(/.*)?"
sudo restorecon -Rv /var/www/html/wordpress
```
> Agora, crie um novo arquivo de configuração do nginx wordpress.conf para o WordPress com o seguinte comando:

`sudo vi /etc/nginx/conf.d/wordpress.conf`

>digite as seguintes linhas de códigos no arquivo wordpress.conf. 

```
server {
    listen 80;
    server_name blog.brunogomessanches.tk;

    root /var/www/html/wordpress;
    index index.php;

    access_log /var/log/nginx/wordpress-access.log;
    error_log /var/log/nginx/wordpress-error.log;
    
     location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_pass unix:/run/php-fpm/www.sock;
        fastcgi_index   index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|ico|svg)$ {
        expires max;
        log_not_found off;
    }

}
```
 
> salvar e sair do arquivo.

> Agora, reinicie o serviço nginx com o comando abaixo e verifique o status do serviço.

```
sudo systemctl restart nginx
sudo systemctl status nginx
```

> abra seu navegador e entre no site , no meu caso wordpress.brunogomessanches.tk clique em let's go,
> devera abrir a pagina de configuração do database , configure com as informaçoes .


**Install e Configurar Tomcat proxy reverso**

>Instale o Ansible em seu sistema usando os comandos :

`sudo yum -y install epel-release && sudo yum -y install ansible`

> Clone o script Ansible do Github

```
cd /tmp/
git clone https://github.com/jmutai/tomcat-ansible.git
cd tomcat-ansible
```

> Atualize seu inventário, por exemplo:
```
vim hosts
[tomcat-nodes]
192.168.20.55 # Adicionar endereço IP do servidor, uma linha por servidor
```

> Atualizar variáveis no playbook file - definir a versão do Tomcat, usuário remoto e credenciais de acesso da IU do Tomcat .

```
vim tomcat-setup.yml
- name: Tomcat deployment playbook
  hosts: tomcat-nodes       
  become: yes               
  become_method: sudo       
  remote_user: root         
  vars:
    tomcat_ver: 9.0.30              
    ui_manager_user: manager        
    ui_manager_pass: password      
    ui_admin_username: admin        
    ui_admin_pass: password         
  roles:
    - tomcat
```

> Installando Tomcat 9 como o ansible

`ansible-playbook -i hosts tomcat-setup.yml`

> Teste a instalação do Tomcat
> Visite o URL do servidor na porta 8080. para testar a instalação e configuração do tomcat.

Apos a configuração , vamos criar o arquivo do nginx direncionando para porta :8080

```
vim /etc/nginx/conf.d/tomcat.conf

server {
  listen 80;

  server_name tomcat.brunogomessanches.tk;
  access_log /var/log/nginx/tomcat-access.log;
  error_log /var/log/nginx/tomcat-error.log;

  location / {
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://127.0.0.1:8080/;
  }
}
```

>Realize o teste .

**Site simples** 

> Ciar a pasta com os arquivos do site 

`mkdir /var/www/html/site`

> vamos criar o arquivo de configuração do virtual host

```
vim /etc/nginx/conf.d/site.conf

server {
    server_name site.brunogomessanches.tk;
    root /var/www/html/site;
    index index.php;

    access_log /var/log/nginx/site-access.log;
    error_log /var/log/nginx/site-error.log;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_pass unix:/run/php-fpm/www.sock;
        fastcgi_index   index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|ico|svg)$ {
        expires max;
        log_not_found off;
    }
}
```

**Install SSL Encrypt**

> você precisará baixar o cliente certbot em seu servidor. Você pode baixar e definir a permissão correta executando o seguinte comando:

```
wget https://dl.eff.org/certbot-auto
 mv certbot-auto /usr/local/bin/certbot-auto
 chown root /usr/local/bin/certbot-auto
 chmod 0755 /usr/local/bin/certbot-auto
```
> Agora, execute o seguinte comando para obter e instalar um certificado SSL para o seu site , altere o site para o desejado .

`/usr/local/bin/certbot-auto --nginx -d loja.brunogomessanches.tk`






teste todos os links .





























